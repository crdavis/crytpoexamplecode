Example code illustrating the following:  
* Symmetric and asymmetric keys generation
* Encryption/decryption of strings and files using AES in GCM mode
* Encryption/decryption of strings and files using asymmetic key cryptography
* Generation and verification of digital signature using Elliptic curve digital signature algorithm
* HMAC generation and verification
* Password hashing and verification using Argon2

### Instructions for compiling and running   
The code can be compile and run from an IDE or via command line.   
   
#### Compile and run using an IDE   
_To compile_: click the "Clean and Build Project" button.   
_To run_: click the "Run Project" button.   
   
### Compile and run using the command line   
_To compile_: mvn compile   
_To run_: mvn exec:java -Dexec.mainClass=com.mycompany.cryptoexamplecode.SymmetricKeyCryptoExample  

 
