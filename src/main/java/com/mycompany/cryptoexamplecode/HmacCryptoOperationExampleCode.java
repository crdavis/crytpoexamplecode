package com.mycompany.cryptoexamplecode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Carlton Davis
 */
/**
 * Example code illustrating HMAC generation, saving to file, and verification.
 */
public class HmacCryptoOperationExampleCode {
    
     //Method to generate HMAC.
    String generateHMac(SecretKey secretKey, String message, String algorithm) 
            throws NoSuchAlgorithmException {
        
        String hash = null;
        try {
            //Get instance of HMAC algorithm.
            Mac mac = Mac.getInstance(algorithm);
            
            //Convert SecretKey to bytes
            byte[] secretKeyBytes = secretKey.getEncoded();
            
            //Generate and initialize the HMAC key.
            SecretKeySpec keySpec = new SecretKeySpec(secretKeyBytes, algorithm);
            mac.init(keySpec);
            
            //Generate the hash using the secret key and the message
            byte[] hmac = mac.doFinal(message.getBytes());
            
            //Encode the message authentication code as Base65 string.
            hash = Base64.getEncoder().encodeToString(hmac);
            System.out.println("\nHMAC for message: " + message + "\n" + hash);
            
        } catch (IllegalStateException | InvalidKeyException | NoSuchAlgorithmException e) {
            System.out.println("\nERROR occured while generating HMAC.");
        }
        
        return hash;
    }
    
    //Method to save HMAC to file
    void saveHmacToFile(String hmac, String path) 
            throws FileNotFoundException, IOException {
       
        File file = new File(path);
        //Create the directory
        file.getParentFile().mkdir();
        
        try (FileOutputStream fos = new FileOutputStream(file)) {
            
            //Write the bytes to the file.
            fos.write(hmac.getBytes());
            
            //Flush and close the FileOutputStream.
            fos.flush();
        }
    }
    
    
    //Method to verify if two HMAC matches.
    boolean areHmacsTheSame(String path, String hmac, String algoritym) 
            throws FileNotFoundException, IOException {
        
        //Create the File object.
        File file = new File(path);
        
        //Create input stream
        FileInputStream inputStream = new FileInputStream(file);
        
        //Read the bytes from the file
        byte[] hashBytes = inputStream.readAllBytes();
        
        String hash = new String(hashBytes);
        
        System.out.println("\nHash retrived from file: " + hash);
        
        if(hash.equals(hmac)) {
            System.out.println("\nVerification of HMAC successful");
            return true;
        } else {
            System.out.println("\nVerification of HMAC FAILed!!!");
            return false;
        }
    }
    
}
