package com.mycompany.cryptoexamplecode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Carlton Davis
 */
/**
 * Example code illustrating asymmetric key generating, encryption
 * and decryption.
 */
public class AsymmetricKeyCryptoExample {
    
  /**
     * Method to generate RSA key pair.
     */
    KeyPair do_GenerateKeyPair(String algorithm, int keySize)
            throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance(algorithm);
        generator.initialize(keySize);

        return generator.generateKeyPair();
    }

    /**
     * This method takes a key and a filename with path and save key in the
     * given file.
     */
    void saveKeysToFile(byte[] key, String path) throws FileNotFoundException, IOException {
        File file = new File(path);
        //Create the directory
        file.getParentFile().mkdir();

        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(key);
            
            //Flush and close the FileOutputStream.
            fos.flush();
        }
    }
    
    
    /**
     * Private key is encoded in PKCS#8 format; thus, the key needs to be
     * generated from base64 encoded string.
     */
    PrivateKey getPrivateKeyFromFile(File privateKeyFile, String algorithm) 
            throws FileNotFoundException, IOException, NoSuchAlgorithmException, 
            InvalidKeySpecException {
        
        //Create input stream
        FileInputStream inputStream = new FileInputStream(privateKeyFile);
        byte[] privateKeyBytes = inputStream.readAllBytes();
        
        //The KeyFactory is then used to recreate the key instance
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        PrivateKey privatekey = keyFactory.generatePrivate(keySpec);
        
        //Print the private key
        System.out.println("\nPrivate key: " + privatekey.toString());
        return privatekey;
    }
    
     /**
     * Public key is encoded in X.509 format; thus, the key needs to be
     * generated from base64 encoded string.
     */
    PublicKey getPublicKeyFromFile(File publicKeyFile, String algorithm) 
            throws FileNotFoundException, IOException, NoSuchAlgorithmException, 
            InvalidKeySpecException {
        
        //Create input stream
        FileInputStream inputStream = new FileInputStream(publicKeyFile);
        byte[] publicKeyBytes = inputStream.readAllBytes();
        
        //The KeyFactory is then used to recreate the key instance
        KeyFactory keyFactory = KeyFactory.getInstance(algorithm);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        PublicKey publickey = keyFactory.generatePublic(keySpec);
        
        //Print the public key
        System.out.println("\nPublic key: " + publickey.toString());
        
        return publickey;
    }
    

    /**
     * This method takes a String to be encrypted, an algorithm and a public
     * key, then returns the encrypted message in Base64 format.
     */
    String asymmetricKeyEncryp(String data, String algorithm, PublicKey publicKey)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //Initializes the Cipher
        Cipher encryptCipher = Cipher.getInstance(algorithm);
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        /**
         * The doFinal method only accepts byte array arguments. Therefore, need
         * to convert the string to bytes.
         */
        byte[] dataBytes = data.getBytes();

        //Encyrpt the message
        byte[] cipherText = encryptCipher.doFinal(dataBytes);

        /**
         * Encode the message in Base64 encoded string such that it will 
         * be easier to store in a database or send via a network.
         */
        String encodedCipherText = Base64.getEncoder().encodeToString(cipherText);

        return encodedCipherText;
    }

    /**
     * This method takes an asymmetric key ciphertext to be decrypted, an
     * algorithm and a private key, then returns the decrypted message.
     */
    String asymmetricKeyDecryp(String cipherText, String algorithm, PrivateKey privateKey)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        //Initializes the Cipher
        Cipher decryptCipher = Cipher.getInstance(algorithm);
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        //Decrypt the ciphertext
        byte[] message = decryptCipher.doFinal(Base64.getDecoder().decode(cipherText));

        return new String(message);
    }

    //Method to encrypt file usiing asymmetric key crypto
    void asymmetricKeyFileEncrypt(String algorithm, PublicKey publicKey, File inputFile, File outputFile)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, FileNotFoundException, IOException,
            IllegalBlockSizeException, BadPaddingException {

        //Initializes the Cipher
        Cipher encryptCipher = Cipher.getInstance(algorithm);
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        FileOutputStream outputStream;
        //Create output stream
        try (
                FileInputStream inputStream = new FileInputStream(inputFile)) {
            //Create output stream
            outputStream = new FileOutputStream(outputFile);
            byte[] buffer = new byte[64];
            int bytesRead;
            //Read up to 64 bytes of data at a time
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                //Cipher.update method takes byte array, input offset and input lentth
                byte[] output = encryptCipher.update(buffer, 0, bytesRead);
                if (output != null) {
                    //Write the ciphertext for the buffer to the output file
                    outputStream.write(output);
                }
            }   //Encrypt the last buffer of plaintext 
            byte[] output = encryptCipher.doFinal();
            if (output != null) {
                outputStream.write(output);
            }
            //Close the input and output streams
        }
        outputStream.close();
    }
    
    
     //Method to encrypt file usiing asymmetric key crypto
    void asymmetricKeyFileDecrypt(String algorithm, PrivateKey privateKey, File inputFile, File outputFile)
            throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, FileNotFoundException, IOException,
            IllegalBlockSizeException, BadPaddingException {

        //Initializes the Cipher
        Cipher encryptCipher = Cipher.getInstance(algorithm);
        encryptCipher.init(Cipher.DECRYPT_MODE, privateKey);

        FileOutputStream outputStream;
        //Create output stream
        try (
                FileInputStream inputStream = new FileInputStream(inputFile)) {
            //Create output stream
            outputStream = new FileOutputStream(outputFile);
            byte[] buffer = new byte[64];
            int bytesRead;
            //Read up to 64 bytes of data at a time
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                //Cipher.update method takes byte array, input offset and input lentth
                byte[] output = encryptCipher.update(buffer, 0, bytesRead);
                if (output != null) {
                    //Write the ciphertext for the buffer to the output file
                    outputStream.write(output);
                }
            }   //Encrypt the last buffer of plaintext 
            byte[] output = encryptCipher.doFinal();
            if (output != null) {
                outputStream.write(output);
            }
            //Close the input and output streams
        }
        outputStream.close();
    }

    
    /**
     * This method generates an Elliptic curve with Digital Signature 
     * Algorithm (ECDSA). Current supported curve names: secp256r1,
     * secp384r1 and secp521r1
     */
    KeyPair generateKeyPairECDSA(String curveName) 
            throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, NoSuchProviderException {
        
        ECGenParameterSpec ecParaSpec = new ECGenParameterSpec(curveName);
        
        /**
         * getInstance method of the key pair generator takes the label "EC"
         * and the Provider ("SunEC") for the Crypto schemes.
         */
        KeyPairGenerator generator = KeyPairGenerator.getInstance("EC", "SunEC");
        generator.initialize(ecParaSpec);
        
        //Generate the key pair
        KeyPair keypair = generator.genKeyPair();
        
        return keypair;
    }
    
    
    /**
     * Method for generating digital signature.
     */
    byte[] generateSignature (String algorithm, PrivateKey privatekey, String message) 
            throws NoSuchAlgorithmException, NoSuchProviderException, 
            InvalidKeyException, UnsupportedEncodingException, SignatureException {
        
        //Create an instance of the signature scheme for the given signature algorithm
        Signature sig = Signature.getInstance(algorithm, "SunEC");
        
        //Initialize the signature scheme
        sig.initSign(privatekey);
        
        //Compute the signature
        sig.update(message.getBytes("UTF-8"));
        byte[] signature = sig.sign();
        
        return signature;
    }
    
    
    /**
     * Method for verifying digital signature.
     */
    boolean verifySignature(byte[] signature, PublicKey publickey, String algorithm, String message) 
            throws NoSuchAlgorithmException, NoSuchProviderException, 
            InvalidKeyException, UnsupportedEncodingException, SignatureException {
        
        //Create an instance of the signature scheme for the given signature algorithm
        Signature sig = Signature.getInstance(algorithm, "SunEC");
        
        //Initialize the signature verification scheme.
        sig.initVerify(publickey);
        
        //Compute the signature.
        sig.update(message.getBytes("UTF-8"));
        
        //Verify the signature.
        boolean validSignature = sig.verify(signature);
        
        if(validSignature) {
            System.out.println("\nSignature is valid");
        } else {
            System.out.println("\nSignature is NOT valid!!!");
        }
        
        return validSignature;
    }
}
