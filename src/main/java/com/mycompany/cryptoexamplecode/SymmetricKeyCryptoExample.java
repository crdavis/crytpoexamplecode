package com.mycompany.cryptoexamplecode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Carlton Davis
 */
/**
 * *
 * Example code illustrating Symmetric key generation, encryption/decryption of
 * strings and files using AES in GCM mode.
 */
public class SymmetricKeyCryptoExample {

    public static final int GCM_IV_LENGTH = 12;
    public static final int GCM_TAG_LENGTH = 16;

    /**
     * *
     * Method for generating secret key takes an integer n; n can be 128, 192 or
     * 256.
     */
    SecretKey GenerateKey(int n) throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(n); //Initialize the key generator
        SecretKey key = keyGenerator.generateKey(); //Generate the key
        return key;
    }

    //Method for generating 12 byte GCM Initialization Vector. 
    byte[] generateGCMIV() {
        byte[] GCMIV = new byte[GCM_IV_LENGTH];
        SecureRandom random = new SecureRandom();
        random.nextBytes(GCMIV);
        return GCMIV;
    }

    /**
     * *
     * Method to encrypt strings: takes an algorithm, string data, a secret key
     * and IV.
     */
    String encryptStr(String algorithm, String data, SecretKey key, byte[] IV)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException {

        //Create an instance of the Cipher class
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        // Initialize Cipher for ENCRYPT_MODE
        cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec);

        //Perform encryption
        byte[] cipherText = cipher.doFinal(data.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);

    }

    //Method for decrypting an input string
    String decryptStr(String algorithm, String cipherText,
            SecretKey key, byte[] IV)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException {
        //Create an instance of the Cipher class.
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        // Initialize Cipher for ENCRYPT_MODE
        cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec);

        //Perform decryption
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));

        return new String(plainText);
    }

    // Method to encrypt a file
    void encryptFile(String algorithm, SecretKey key, byte[] IV, File inputFile, File outputFile)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            FileNotFoundException, IOException {

        //Create an instance of the Cipher class
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        cipher.init(Cipher.ENCRYPT_MODE, key, gcmParameterSpec);

        FileOutputStream outputStream;
        //Create output stream
        try (
                FileInputStream inputStream = new FileInputStream(inputFile)) {
            //Create output stream
            outputStream = new FileOutputStream(outputFile);
            byte[] buffer = new byte[64];
            int bytesRead;
            //Read up to 64 bytes of data at a time
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                //Cipher.update method takes byte array, input offset and input lentth
                byte[] output = cipher.update(buffer, 0, bytesRead);
                if (output != null) {
                    //Write the ciphertext for the buffer to the output file
                    outputStream.write(output);
                }
            }   //Encrypt the last buffer of plaintext 
            byte[] output = cipher.doFinal();
            if (output != null) {
                outputStream.write(output);
            }
            //Close the input and output streams
        }
        outputStream.close();
    }

    //Method to decrypt a file
    void decryptFile(String algorithm, SecretKey key,
            byte[] IV, File inputFile,
            File outputFile)
            throws NoSuchAlgorithmException, BadPaddingException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            FileNotFoundException, IOException {

        //Create an instance of the Cipher class
        Cipher cipher = Cipher.getInstance(algorithm);

        // Create GCMParameterSpec
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV);

        cipher.init(Cipher.DECRYPT_MODE, key, gcmParameterSpec);

        FileOutputStream outputStream;
        //Create output stream
        try (
                FileInputStream inputStream = new FileInputStream(inputFile)) {
            //Create output stream
            outputStream = new FileOutputStream(outputFile);
            byte[] buffer = new byte[64];
            int bytesRead;
            //Read up to 64 bytes of data at a time
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byte[] output = cipher.update(buffer, 0, bytesRead);
                if (output != null) {
                    //Write the Plaintext to the output file
                    outputStream.write(output);
                }
            }   //Decrypt the last buffer of ciphertext
            byte[] output = cipher.doFinal();
            if (output != null) {
                outputStream.write(output);
            }
            //Close the input and output streams.
        }
        outputStream.close();
    }

    //Driver code
    public static void main(String[] args) throws NoSuchAlgorithmException,
            BadPaddingException,
            NoSuchPaddingException,
            InvalidKeyException,
            InvalidAlgorithmParameterException,
            IllegalBlockSizeException,
            IOException,
            FileNotFoundException,
            InvalidKeySpecException,
            NoSuchProviderException,
            UnsupportedEncodingException,
            SignatureException {

        SymmetricKeyCryptoExample cryptoUtil = new SymmetricKeyCryptoExample();

        //Generate a 256-bit secret key.
        var key = cryptoUtil.GenerateKey(256);

        //Generate GCM IV.
        byte[] iv = cryptoUtil.generateGCMIV();

        String algorithm = "AES/GCM/NoPadding";

        //The text to be encrypted
        String input = "Text to be encrypted";
        System.out.println("\nplaintext: " + input);

        //Get ciphertext
        String cipherText = cryptoUtil.encryptStr(algorithm, input, key, iv);
        System.out.println("\nciphertext: " + cipherText);

        //Decrypt cipherText
        String plainText = cryptoUtil.decryptStr(algorithm, cipherText, key, iv);
        System.out.println("\nText after decryption: " + plainText);

        //File to encrypted
        String file = "README.md";
        File inputFile = new File(file);

        //File to store cipherText
        String outputFileName = "README.md.enc";
        File outputFile = new File(outputFileName);

        //Encrypt the file
        cryptoUtil.encryptFile(algorithm, key, iv, inputFile, outputFile);

        //File to store decrypted content
        String fileD = "README.md.dec";
        File fileDecrypted = new File(fileD);

        //Decrypt the file
        cryptoUtil.decryptFile(algorithm, key, iv, outputFile, fileDecrypted);

        /* Driver code for Asymmetric key crypto operations. */
        AsymmetricKeyCryptoExample asymmetricKeyUtil = new AsymmetricKeyCryptoExample();

        System.out.println("\n\n****** Output for asymmetric crypto operation follows ****.");

        //Generate 2048-bit RSA key pair.
        String algorithm_A = "RSA";
        KeyPair keypair;
        keypair = asymmetricKeyUtil.do_GenerateKeyPair(algorithm_A, 2048);

        //Derive the private and public key from the key pair.
        PrivateKey privatekey = keypair.getPrivate();
        PublicKey publickey = keypair.getPublic();

        //Save the private key to a file
        String privateKeyFile = "RSA/private.key";
        asymmetricKeyUtil.saveKeysToFile(privatekey.getEncoded(), privateKeyFile);

        //Save the public key to a file
        String publicKeyFile = "RSA/public.key";
        asymmetricKeyUtil.saveKeysToFile(publickey.getEncoded(), publicKeyFile);

        //Get the public key from file
        File publicKeyFileObj = new File(publicKeyFile);
        PublicKey publicKeyFromFile
                = asymmetricKeyUtil.getPublicKeyFromFile(publicKeyFileObj, algorithm_A);

        //The text to be encrypted by asymmetric key scheme.
        String message = "Text to be encrypted";
        System.out.println("\nPlaintext for asymmetric key scheme: " + message);

        //Encrypt message using public key obtained from file.
        String encryptedMsg = asymmetricKeyUtil
                .asymmetricKeyEncryp(message, algorithm_A, publicKeyFromFile);
        System.out.println("\nAsymmetric key encrypted message: " + encryptedMsg);

        //Get the private key from file
        File privateKeyFileObj = new File(privateKeyFile);
        PrivateKey privateKeyFromFile
                = asymmetricKeyUtil.getPrivateKeyFromFile(privateKeyFileObj, algorithm_A);

        //Decrypt message using public key obtained from file.
        String decryptedMsg = asymmetricKeyUtil
                .asymmetricKeyDecryp(encryptedMsg, algorithm_A, privateKeyFromFile);
        System.out.println("\nAsymmetric key decrypted message: " + decryptedMsg);

        //File to encrypt with public key (not the one from the file).
        String fileA = "EncriptMe.txt";
        File inputFileA = new File(fileA);

        //File to store encrypted data
        String outputFileNameA = "EncriptMe.txt-asymmetric-encrypt";
        File outputFileA = new File(outputFileNameA);

        //Encrypt the file with the public key (not the one obtained from the file).
        asymmetricKeyUtil.asymmetricKeyFileEncrypt(algorithm_A, publickey, inputFileA, outputFileA);

        //File to store decrypted data
        String fileD_A = "EncriptMe.txt-asymmetric.dec";
        File fileDecrypted_A = new File(fileD_A);

        //Decrypt the file with the private key (not the one obtained from the file).
        asymmetricKeyUtil.asymmetricKeyFileDecrypt(algorithm_A, privatekey, outputFileA, fileDecrypted_A);

        /* Driver code for password hashing */
        System.out.println("\n\n********* Output for password hashing follows **********");

        Argon2HashingExampleCode hashingObj = new Argon2HashingExampleCode();

        //Assume secure method used to get password from user
        char[] password = "Password that is difficult to guess".toCharArray();
        String passwordHash = hashingObj.getArgon2Hash(10, 65536, 1, password);

        System.out.println("\nArgon2 hash of password: " + passwordHash);
        System.out.println("password hash length: " + passwordHash.length());

        //Assume that user enters the following password
        String password2 = "Password that is difficult to guess";

        //Verify if the password is correct
        if (hashingObj.verifyPassword(passwordHash, password2)) {
            System.out.println("\nPassword verfication successful!");
        } else {
            System.out.println("\nPassword verfication NOT successful!");
        }
        
        //Compute ShA3-256 hash
        String hashAlgorithm = "SHA3-256";
        String msgToHash = "This is the message to hash";
        byte[] hashBytes = hashingObj.computeHash(hashAlgorithm, msgToHash);
        String hash = Base64.getEncoder().encodeToString(hashBytes);
        System.out.println("\nMessage to hash: " + msgToHash +"\nSHA3 hash: "
                + hash);

        /* Driver code for HMAC generation and verification */
        System.out.println("\n\n********* Output for HMAC operations follows **********");

        String msg = "Message whose integrity needs to be verified";

        //Generate a 256 bit secret key
        var hmacKey = cryptoUtil.GenerateKey(256);

        //Create an object of HmacCryptoOperationExampleCode class.
        HmacCryptoOperationExampleCode hmacUtil = new HmacCryptoOperationExampleCode();

        //Possible choices of HMAC algorithms are HMACSHA256, HMACSHA384, HMACSHA512. 
        String hmacAlgorithm = "HMACSHA256";

        //Generate the HMAC for the message using the secret key.
        String hmac = hmacUtil.generateHMac(hmacKey, msg, hmacAlgorithm);

        //Save the HMAC to a file
        String hmacFile = "HMAC/hmac.txt";
        hmacUtil.saveHmacToFile(hmac, hmacFile);

        //Message received
        String msg2 = "Message whose integrity needs to be verified";

        //Generate the HMAC for the received message.
        String hmac2 = hmacUtil.generateHMac(hmacKey, msg2, hmacAlgorithm);

        //Verify if HMAC are the same.
        hmacUtil.areHmacsTheSame(hmacFile, hmac2, hmacAlgorithm);

        /* Driver code for digital signature generation and verification */
        System.out.println("\n\n********* Output for digital signature follows **********");

        String ecCurveName = "secp256r1";
        String ecDsaAlgorithm = "SHA256withECDSA";

        //Generate ECDSA key pair.
        KeyPair ecDsaKeyPair = asymmetricKeyUtil.generateKeyPairECDSA(ecCurveName);

        //Get the public key
        PublicKey ecDsaPublicKey = ecDsaKeyPair.getPublic();
        System.out.println("ECDSA public key: " + Base64.getEncoder()
                .encodeToString(ecDsaPublicKey.getEncoded()));

        //Get the private key
        PrivateKey ecDsaPrivateKey = ecDsaKeyPair.getPrivate();
        
        /**
         * NB: Private key should never be printed. This is meant for you
         * to see what the private key looks like. This should never be done
         * in practice.
         */
        System.out.println("\nECDSA private key: " + Base64.getEncoder()
                .encodeToString(ecDsaPrivateKey.getEncoded()));


        String msgToBeSigned = "This is the message to be signed.";
        String receivedMsg = "This is the message to be signed.";
        
        //Generate signature for the message.
        byte[] signature = asymmetricKeyUtil.generateSignature(ecDsaAlgorithm, ecDsaPrivateKey, msgToBeSigned);
        System.out.println("\nSignature: " + Base64.getEncoder().encodeToString(signature));
        
        //Validate the signature
       asymmetricKeyUtil.verifySignature(signature, ecDsaPublicKey, ecDsaAlgorithm, receivedMsg);

    }

}
