package com.mycompany.cryptoexamplecode;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Carlton Davis
 */
/**
 *
 * Code illustrating use of Argon2 for password hashing
 */
public class Argon2HashingExampleCode {

    /**
     *
     * @param iterations: Number of iterations.
     * @param memory: Sets memory usage to x kibibytes.
     * @param parallelism: Number of threads and compute lanes.
     * @param password: Password to hash.
     * @return hash of password.
     */
    String getArgon2Hash(int iterations, int memory, int parallelism, char[] password) {

        /**
         * Create instance of Argon2. The default instance has the following
         * characteristics: Argon2Types.ARGON2i Length of salt: 16 bytes Hash
         * length: 32 bytes Can be changed with: Argon2 argon2 = Argon2Factory
         * .create(Argon2Factory.Argon2Types.ARGON2id, 32, 64);
         */
        //Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id, 32, 64);
        Argon2 argon2 = Argon2Factory.create();

        /**
         * Generate password hash
         */
        String passwordHash = "";

        try {
            passwordHash = argon2.hash(iterations, memory, parallelism, password);

        } finally {
            // Wipe confidential data
            argon2.wipeArray(password);
        }

        return passwordHash;
    }

    //Verifies if password hash matches a stored hash
    boolean verifyPassword(String hash, String password) {

        //Create an instance of Argon2
        Argon2 argon2 = Argon2Factory.create();

        try {

            if (argon2.verify(hash, password.toCharArray())) {
                System.out.println("\n\nHash matches password.");
                return true;
            } else {
                System.out.println("\n\nHash does NOT matches password.");
                return false;
            }
        } finally {
            // Wipe confidential data
            argon2.wipeArray(password.toCharArray());
        }
    }
    
    
    /**
     * ShA3-256 hashing.
     * Possible algorithm choices includes "SHA-256" "SHA-512", 
     * "SHA3-256" and "SHA3-512".
     */
    byte[] computeHash(String algorithm, String message) 
            throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        byte[] hashbytes = digest.digest(message.getBytes(StandardCharsets.UTF_8));
        
        return hashbytes;
    }
}
